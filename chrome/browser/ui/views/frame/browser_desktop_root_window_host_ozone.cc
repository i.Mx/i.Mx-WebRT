// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "chrome/browser/ui/views/frame/browser_desktop_root_window_host_ozone.h"

#include "chrome/browser/themes/theme_service.h"
#include "chrome/browser/themes/theme_service_factory.h"
#include "chrome/browser/ui/views/frame/browser_frame.h"
#include "chrome/browser/ui/views/frame/browser_view.h"

namespace {

// DesktopThemeProvider maps resource ids using MapThemeImage(). This is
// necessary for BrowserDesktopWindowTreeHostWin so that it uses the windows
// theme images rather than the ash theme images.
//
// This differs from the version in browser_desktop_window_tree_host_win.cc
// because we need to also look up whether we're using the native theme.
class DesktopThemeProvider : public ui::ThemeProvider {
 public:
  explicit DesktopThemeProvider(const ui::ThemeProvider& delegate)
      : delegate_(delegate) {
  }

  gfx::ImageSkia* GetImageSkiaNamed(int id) const override {
    return delegate_.GetImageSkiaNamed(id);
  }
  SkColor GetColor(int id) const override { return delegate_.GetColor(id); }
  int GetDisplayProperty(int id) const override {
    return delegate_.GetDisplayProperty(id);
  }
  bool ShouldUseNativeFrame() const override {
    return delegate_.ShouldUseNativeFrame();
  }
  bool HasCustomImage(int id) const override {
    return delegate_.HasCustomImage(id);
  }
  base::RefCountedMemory* GetRawData(int id, ui::ScaleFactor scale_factor)
      const override {
    return delegate_.GetRawData(id, scale_factor);
  }

 private:
  const ui::ThemeProvider& delegate_;

  DISALLOW_COPY_AND_ASSIGN(DesktopThemeProvider);
};

} // namespace

////////////////////////////////////////////////////////////////////////////////
// BrowserDesktopWindowTreeHostOzone, public:

BrowserDesktopWindowTreeHostOzone::BrowserDesktopWindowTreeHostOzone(
    views::internal::NativeWidgetDelegate* native_widget_delegate,
    views::DesktopNativeWidgetAura* desktop_native_widget_aura,
    BrowserView* browser_view,
    BrowserFrame* browser_frame)
    : DesktopWindowTreeHostOzone(native_widget_delegate,
                                   desktop_native_widget_aura),
      browser_view_(browser_view) {
}


BrowserDesktopWindowTreeHostOzone::~BrowserDesktopWindowTreeHostOzone() {
}

////////////////////////////////////////////////////////////////////////////////
// BrowserDesktopWindowTreeHostOzone,
//     BrowserDesktopWindowTreeHost implementation:

views::DesktopWindowTreeHost*
    BrowserDesktopWindowTreeHostOzone::AsDesktopWindowTreeHost() {
  return this;
}

int BrowserDesktopWindowTreeHostOzone::GetMinimizeButtonOffset() const {
  return 0;
}

bool BrowserDesktopWindowTreeHostOzone::UsesNativeSystemMenu() const {
  return false;
}

////////////////////////////////////////////////////////////////////////////////
// BrowserDesktopWindowTreeHostOzone,
//     views::DesktopWindowTreeHostOzone implementation:

void BrowserDesktopWindowTreeHostOzone::Init(
    aura::Window* content_window,
    const views::Widget::InitParams& params) {
  views::DesktopWindowTreeHostOzone::Init(content_window, params);

  // TODO(kalyan): Support for Global Menu.
}

void BrowserDesktopWindowTreeHostOzone::CloseNow() {
  views::DesktopWindowTreeHostOzone::CloseNow();
}

////////////////////////////////////////////////////////////////////////////////
// BrowserDesktopWindowTreeHost, public:

// static
BrowserDesktopWindowTreeHost*
    BrowserDesktopWindowTreeHost::CreateBrowserDesktopWindowTreeHost(
        views::internal::NativeWidgetDelegate* native_widget_delegate,
        views::DesktopNativeWidgetAura* desktop_native_widget_aura,
        BrowserView* browser_view,
        BrowserFrame* browser_frame) {
  return new BrowserDesktopWindowTreeHostOzone(native_widget_delegate,
                                               desktop_native_widget_aura,
                                               browser_view,
                                               browser_frame);
}
